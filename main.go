package main

import (
	"bitbucket.org/trevorrobertsjr/psqlgo/handlers"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq"
)

var db *sql.DB

const (
	dbhost = "DBHOST"
	dbport = "DBPORT"
	dbuser = "DBUSER"
	dbpass = "DBPASS"
	dbname = "DBNAME"
)

// type repositorySummary struct {
// 	ID         int
// 	Name       string
// 	Owner      string
// 	TotalStars int
// }

// type repositories struct {
// 	Repositories []repositorySummary
// }

// repository contains the details of a repository
type mlbInfo struct {
	Mlb_id   int
	Mlb_name string
	Mlb_pos  string
	Mlb_team string
}

type mlbTeams struct {
	MlbTeams []mlbInfo
}

func main() {
	initDb()
	defer db.Close()
	http.HandleFunc("/api/playerremote", handlers.PlayerHandler)
	http.HandleFunc("/api/teamremote/", handlers.TeamHandler)
	http.HandleFunc("/api/player", playerHandler)
	http.HandleFunc("/api/team/", teamHandler)
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}

func playerHandler(w http.ResponseWriter, r *http.Request) {
	//...
	mlbrepos := mlbTeams{}

	err := queryRepos(&mlbrepos)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	out, err := json.Marshal(mlbrepos)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	// fmt.Printf("%s\n", out)
	fmt.Fprintf(w, string(out))
}

// queryRepos first fetches the repositories data from the db
func queryRepos(mlbrepos *mlbTeams) error {
	rows, err := db.Query(`
		SELECT
			mlb_id,  
            mlb_name,
            mlb_pos,
            mlb_team
		FROM dms_sample.mlb_data
		ORDER BY mlb_team DESC
		limit 10`)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		team := mlbInfo{}
		err = rows.Scan(
			&team.Mlb_id,
			&team.Mlb_name,
			&team.Mlb_pos,
			&team.Mlb_team,
		)
		if err != nil {
			return err
		}
		mlbrepos.MlbTeams = append(mlbrepos.MlbTeams, team)
		// fmt.Printf("%s\n", mlbrepos.MlbTeams[0].mlb_name)
	}
	err = rows.Err()
	if err != nil {
		return err
	}
	return nil
}
func teamHandler(w http.ResponseWriter, r *http.Request) {
	//...

}

func initDb() {
	config := dbConfig()
	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		config[dbhost], config[dbport],
		config[dbuser], config[dbpass], config[dbname])

	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	fmt.Println("Successfully connected!")
}

func dbConfig() map[string]string {
	conf := make(map[string]string)
	host, ok := os.LookupEnv(dbhost)
	if !ok {
		panic("DBHOST environment variable required but not set")
	}
	port, ok := os.LookupEnv(dbport)
	if !ok {
		panic("DBPORT environment variable required but not set")
	}
	user, ok := os.LookupEnv(dbuser)
	if !ok {
		panic("DBUSER environment variable required but not set")
	}
	password, ok := os.LookupEnv(dbpass)
	if !ok {
		panic("DBPASS environment variable required but not set")
	}
	name, ok := os.LookupEnv(dbname)
	if !ok {
		panic("DBNAME environment variable required but not set")
	}
	conf[dbhost] = host
	conf[dbport] = port
	conf[dbuser] = user
	conf[dbpass] = password
	conf[dbname] = name
	return conf
}
