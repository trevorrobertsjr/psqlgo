#!/bin/bash
# psql -h appdata.cewt4z3whsfj.us-east-1.rds.amazonaws.com -U master -d postgres
export DBHOST=appdata.cewt4z3whsfj.us-east-1.rds.amazonaws.com
export DBPORT=5432
export DBUSER=master
export DBPASS=$1
export DBNAME=postgres